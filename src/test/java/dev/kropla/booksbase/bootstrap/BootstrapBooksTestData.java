package dev.kropla.booksbase.bootstrap;

import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.model.entities.Book;

import java.util.List;
import java.util.UUID;

public class BootstrapBooksTestData {

    public static List<BookDto> getBooksDto() {

        return getBooks().stream()
                .map(b -> BookDto.builder()
                        .title(b.getTitle())
                        .author(b.getAuthor())
                        .pagesNumber(b.getPagesNumber())
                        .rating(b.getRating())
                        .isbn(b.getIsbn())
                        .build()
                ).toList();
    }

    public static List<Book> getBooks() {
        return BootstrapData.generateTestBookData();
    }

    public static BookDto generateSingleBookDtoWithId() {
        var book = BootstrapBooksTestData.getBooksDto().getFirst();
        book.setId(UUID.randomUUID());
        return book;
    }
}
