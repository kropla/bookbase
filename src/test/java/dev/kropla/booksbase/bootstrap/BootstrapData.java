package dev.kropla.booksbase.bootstrap;

import dev.kropla.booksbase.model.entities.Book;
import dev.kropla.booksbase.repositories.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Profile("it")
@Component
@RequiredArgsConstructor
public class BootstrapData implements CommandLineRunner {
    private final BookRepository bookRepository;


    @Override
    public void run(String... args) throws Exception {
        loadBookData();
    }

    private void loadBookData() {
        if (bookRepository.count() == 0) {
            bookRepository.saveAll(generateTestBookData());
        }
    }

    static List<Book> generateTestBookData() {
        List<Book> books = new ArrayList<>();
        books.add(Book.builder()
                .title("Pride and Prejudice")
                .author("Jane Austen")
                .pagesNumber(61)
                .rating(4)
                .isbn("978-08-8301-758-6")
                .build());

        books.add(Book.builder()
                .title("To Kill a Mockingbird")
                .author("Harper Lee")
                .pagesNumber(409)
                .rating(5)
                .isbn("9780060173227")
                .build());

        books.add(Book.builder()
                .title("The Lord of the Rings: The Fellowship of the Ring")
                .author("J.R.R. Tolkien")
                .pagesNumber(606)
                .rating(5)
                .isbn("83-7150-241-9")
                .build());

        books.add(Book.builder()
                .title("Harry Potter and the Sorcerer's Stone")
                .author("J.K. Rowling")
                .pagesNumber(248)
                .rating(4)
                .isbn("978-83-8008-118-5")
                .build());

        books.add(Book.builder()
                .title("Nineteen eighty-four")
                .author("George Orwell")
                .pagesNumber(460)
                .rating(5)
                .isbn("01-9818-521-9")
                .build());

        books.add(Book.builder()
                .title("Animal Farm")
                .author("George Orwell")
                .pagesNumber(112)
                .rating(4)
                .isbn("9780582330870")
                .build());

        books.add(Book.builder()
                .title("The Great Gatsby")
                .author("F. Scott Fitzgerald")
                .pagesNumber(209)
                .rating(5)
                .isbn("9780192832696")
                .build());

        books.add(Book.builder()
                .title("One Hundred Years of Solitude")
                .author("Gabriel García Márquez")
                .pagesNumber(488)
                .rating(5)
                .isbn("9780065023961")
                .build());

        books.add(Book.builder()
                .title("The Catcher in the Rye")
                .author("J.D. Salinger")
                .pagesNumber(275)
                .rating(4)
                .isbn("978-01-4023-750-4")
                .build());

        books.add(Book.builder()
                .title("Invisible Man")
                .author("Ralph Ellison")
                .pagesNumber(568)
                .rating(5)
                .isbn("978 06 7972 313 4")
                .build());
        return books;
    }
}
