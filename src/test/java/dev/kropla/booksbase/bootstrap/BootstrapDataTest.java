package dev.kropla.booksbase.bootstrap;

import dev.kropla.booksbase.repositories.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class BootstrapDataTest {

    @Autowired
    private BookRepository bookRepository;

    private BootstrapData bootstrapData;

    @BeforeEach
    void setUp() {
        bootstrapData = new BootstrapData(bookRepository);
    }

    @Test
    void initLoadDataTest() throws Exception {

        bootstrapData.run();

        assertThat(bookRepository.count()).isEqualTo(10);

    }
}