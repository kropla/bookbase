package dev.kropla.booksbase.model.dto;

import dev.kropla.booksbase.bootstrap.BootstrapBooksTestData;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class BookDtoIsbnValidatorTest {

    private static Validator validator;

    @BeforeAll
    public static void setUp() {

        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        }
    }

    @ParameterizedTest(name = "#{index} - Run Null or blank ISBN test with args={0}")
    @NullSource
    @ValueSource(strings = {"", " "})
    void validate_nullOrBlankISBN_shouldBeInvalid(String isbn) {

        Set<ConstraintViolation<BookDto>> violations = validator.validate(getBookForTest(isbn));

        // resulting constraint violation set contains 2 exceptions as blank string ISBN violates 2 times - incorrect and empty
        assertThat(violations).hasSize(2);
    }

    @ParameterizedTest(name = "#{index} - Run test with args={0}")
    @ValueSource(strings = {"0618129023", "1059035342X", "83-71502419", "01.9818.521.9", "019818 521 9", "abcdefghijk"})
    void validate_incorrectISBN_shouldBeInvalid(String isbn) {

        Set<ConstraintViolation<BookDto>> violations = validator.validate(getBookForTest(isbn));

        assertThat(violations).hasSize(1);
    }

    @ParameterizedTest(name = "#{index} - Run test with args={0}")
    @ValueSource(strings = {"978-088301758-6", "97800601732 27", "978-83-8008-118.5"})
    void validate_incorrectISBN13_shouldBeInvalid(String isbn) {

        Set<ConstraintViolation<BookDto>> violations = validator.validate(getBookForTest(isbn));

        assertThat(violations).hasSize(1);
    }

    @ParameterizedTest(name = "#{index} - Run test with args={0}")
    @ValueSource(strings = {"059035342X", "83-7150-241-9", "01-9818-521-9", "01 9818 521 9", "0198185219"})
    void validate_correctISBN10_shouldBeValid(String isbn) {

        Set<ConstraintViolation<BookDto>> violations = validator.validate(getBookForTest(isbn));

        assertThat(violations).isEmpty();
    }

    @ParameterizedTest(name = "#{index} - Run test with args={0}")
    @ValueSource(strings = {"978-08-8301-758-6", "9780060173227", "978-83-8008-118-5", "978-06-7972-313-4", "9780582330870", "9780192832696", "9780065023961", "978-01-4023-750-4"})
    void validate_correctISBN13WithDash_shouldBeValid(String isbn) {

        Set<ConstraintViolation<BookDto>> violations = validator.validate(getBookForTest(isbn));

        assertThat(violations).isEmpty();
    }

    private static BookDto getBookForTest(String isbn) {
        var book = BootstrapBooksTestData.getBooksDto().getFirst();
        book.setIsbn(isbn);
        return book;
    }
}
