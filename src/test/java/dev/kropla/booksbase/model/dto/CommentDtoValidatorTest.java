package dev.kropla.booksbase.model.dto;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class CommentDtoValidatorTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        }
    }

    @Test
    void validate_correctComment_shouldBeValid() {
        var comment = CommentDto.builder().text("Test comment text").build();

        Set<ConstraintViolation<CommentDto>> violations = validator.validate(comment);
        assertThat(violations).isEmpty();
    }

    @Test
    void validate_tooLargeComment_shouldBeInValid() {
        Random random = new Random();
        var largeText = random.ints('a', 'z')
                .limit(256)
                .mapToObj(Character::toString)
                .collect(Collectors.joining());
        var comment = CommentDto.builder().text(largeText).build();

        Set<ConstraintViolation<CommentDto>> violations = validator.validate(comment);
        assertThat(violations).hasSize(1);
    }

    @Test
    void validate_emptyTextComment_shouldBeInValid() {
        var comment = CommentDto.builder().build();

        Set<ConstraintViolation<CommentDto>> violations = validator.validate(comment);

        assertThat(violations).hasSize(1);
    }

}
