package dev.kropla.booksbase.model.dto;

import dev.kropla.booksbase.bootstrap.BootstrapBooksTestData;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class BookDtoValidatorTest {

    private Validator validator;

    @BeforeEach
    public void setUp() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            validator = factory.getValidator();
        }
    }

    @Test
    void validate_correctBook_shouldBeValid() {
        var book = BootstrapBooksTestData.getBooksDto().getFirst();

        Set<ConstraintViolation<BookDto>> violations = validator.validate(book);
        assertThat(violations).isEmpty();
    }

    @ParameterizedTest(name = "#{index} - Run Null or blank author, title test with args={0}")
    @NullSource
    @ValueSource(strings = {""})
    void validate_blankOrNullValues_shouldBeInValid(String value) {
        var book = BootstrapBooksTestData.getBooksDto().getFirst();
        book.setTitle(value);
        book.setAuthor(value);

        Set<ConstraintViolation<BookDto>> violations = validator.validate(book);

        assertThat(violations).hasSize(2);
    }

    @ParameterizedTest(name = "#{index} - Run incorrect rating value test with args={0}")
    @ValueSource(ints = {-111, 0, 6, 200})
    void validate_ratingInCorrect_shouldBeInValid(int value) {
        var book = BootstrapBooksTestData.getBooksDto().getFirst();
        book.setRating(value);

        Set<ConstraintViolation<BookDto>> violations = validator.validate(book);

        assertThat(violations).hasSize(1);
    }

    @ParameterizedTest(name = "#{index} - Run incorrect pageNumber value test with args={0}")
    @ValueSource(ints = {-111, 0})
    void validate_pagesNumberNegative_shouldBeInValid(int value) {
        var book = BootstrapBooksTestData.getBooksDto().getFirst();
        book.setPagesNumber(value);

        Set<ConstraintViolation<BookDto>> violations = validator.validate(book);

        assertThat(violations).hasSize(1);
    }

}