package dev.kropla.booksbase;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles({"it"})
@SpringBootTest
class BooksBaseApplicationTests {

    @Test
    void contextLoads() {
    }

}
