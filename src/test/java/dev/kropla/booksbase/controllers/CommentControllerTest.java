package dev.kropla.booksbase.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.services.CommentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CommentController.class)
class CommentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CommentService commentService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void addCommentToBook_returnCreatedComment() throws Exception {
        var commentText = "New comment to book";
        var commentId = UUID.randomUUID();
        var bookId = UUID.randomUUID();
        var comment = CommentDto.builder().id(null).text(commentText).build();
        var savedComment = CommentDto.builder().id(commentId).text(commentText).build();

        when(commentService.addComment(bookId, comment)).thenReturn(savedComment);

        mockMvc.perform(post("/books/{bookId}/comments", bookId)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(comment)))
                .andExpect(status().isCreated()).andExpect(header().exists("Location"))
                .andExpect(jsonPath("$.id", is(commentId.toString())))
                .andExpect(jsonPath("$.text", is(commentText)));

        verify(commentService, times(1)).addComment(any(), any());
        verifyNoMoreInteractions(commentService);
    }

    @Test
    void addCommentToBook_notFoundErrorThrown_returnNotFoundStatus() throws Exception {
        when(commentService.addComment(any(), any())).thenThrow(new NotFoundException("404 exception"));

        mockMvc.perform(post("/books/{bookId}/comments", UUID.randomUUID().toString())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(CommentDto.builder().text("test").build())))
                .andExpect(status().isNotFound());

        verify(commentService, times(1)).addComment(any(), any());
        verifyNoMoreInteractions(commentService);
    }

}