package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.repositories.BookRepository;
import dev.kropla.booksbase.repositories.CommentRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

@ActiveProfiles({"it"})
@SpringBootTest
class CommentControllerIT {

    @Autowired
    CommentController commentController;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    CommentRepository commentRepository;


    @Rollback
    @Transactional
    @Test
    void addCommentToBook_bookExists_shouldReturnNewComment() {
        // Given
        var newCommentText = "New comment to book";
        var bookFromDb = bookRepository.findAll().getFirst();
        var newComment = CommentDto.builder().text(newCommentText).build();

        // When
        ResponseEntity<CommentDto> response = commentController.addCommentToBook(bookFromDb.getId(), newComment);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotNull();
        assertThat(response.getBody().getText()).isEqualTo(newCommentText);
    }

    @Rollback
    @Transactional
    @Test
    void addCommentToBook_bookNotExists_shouldThrowNotFoundException() {
        // Given
        var newComment = CommentDto.builder().build();

        // When + Then
        assertThatExceptionOfType(NotFoundException.class)
                .isThrownBy(() -> commentController.addCommentToBook(UUID.randomUUID(), newComment));
    }

}