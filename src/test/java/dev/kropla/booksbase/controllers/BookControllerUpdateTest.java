package dev.kropla.booksbase.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kropla.booksbase.services.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static dev.kropla.booksbase.bootstrap.BootstrapBooksTestData.generateSingleBookDtoWithId;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BookController.class)
class BookControllerUpdateTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void updateBook_bookExists_returnNoContentStatus() throws Exception {
        var book = generateSingleBookDtoWithId();
        var newAuthor = "New author";
        book.setAuthor(newAuthor);
        when(bookService.update(any(), any())).thenReturn(Optional.of(generateSingleBookDtoWithId()));

        mockMvc.perform(put("/books/{id}", book.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isNoContent());

        verify(bookService, times(1)).update(any(), any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    void updateBook_noMatchBook_returnNotFoundStatus() throws Exception {
        var book = generateSingleBookDtoWithId();

        when(bookService.update(any(), any())).thenReturn(Optional.empty());

        mockMvc.perform(put("/books/{id}", book.getId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isNotFound());

        verify(bookService, times(1)).update(any(), any());
        verifyNoMoreInteractions(bookService);
    }
}
