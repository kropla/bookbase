package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.services.BookService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BookController.class)
class BookControllerDeleteTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @Test
    void removeBook_returnNoContent() throws Exception {
        var uuid = UUID.randomUUID().toString();
        when(bookService.removeBook(any())).thenReturn(true);
        ArgumentCaptor<UUID> uuidArgumentCaptor = ArgumentCaptor.forClass(UUID.class);

        mockMvc.perform(delete("/books/{id}", uuid)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        verify(bookService, times(1)).removeBook(any());
        verify(bookService).removeBook(uuidArgumentCaptor.capture());
        verifyNoMoreInteractions(bookService);
        assertThat(uuid).isEqualTo(uuidArgumentCaptor.getValue().toString());
    }

    @Test
    void removeBook_bookNotExists_returnNotFound() throws Exception {
        when(bookService.removeBook(any())).thenReturn(false);

        mockMvc.perform(delete("/books/{id}", UUID.randomUUID().toString())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(bookService, times(1)).removeBook(any());
        verifyNoMoreInteractions(bookService);
    }

}