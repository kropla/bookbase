package dev.kropla.booksbase.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.services.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static dev.kropla.booksbase.bootstrap.BootstrapBooksTestData.generateSingleBookDtoWithId;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BookController.class)
class BookControllerCreateTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void createBook_returnCreatedEntity() throws Exception {
        var book = generateSingleBookDtoWithId();
        book.setId(null);
        when(bookService.create(book)).thenReturn(generateSingleBookDtoWithId());

        mockMvc.perform(post("/books")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isCreated()).andExpect(header().exists("Location"));

        verify(bookService, times(1)).create(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    void createBook_incorrectData_returnBadRequest() throws Exception {
        var book = BookDto.builder().build();

        mockMvc.perform(post("/books")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isBadRequest());

        verifyNoInteractions(bookService);
    }
}
