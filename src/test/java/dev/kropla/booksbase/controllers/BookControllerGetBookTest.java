package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.services.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;
import java.util.UUID;

import static dev.kropla.booksbase.bootstrap.BootstrapBooksTestData.generateSingleBookDtoWithId;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BookController.class)
class BookControllerGetBookTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @Test
    void getBook_recordReturnedByService_returnsBookWithCorrectStructure() throws Exception {
        var book = generateSingleBookDtoWithId();
        when(bookService.getBook(any())).thenReturn(Optional.of(book));

        mockMvc.perform(get("/books/{id}", book.getId().toString()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(book.getId().toString())))
                .andExpect(jsonPath("$.title", is(book.getTitle())))
                .andExpect(jsonPath("$.author", is(book.getAuthor())))
                .andExpect(jsonPath("$.isbn", is(book.getIsbn())))
                .andExpect(jsonPath("$.pagesNumber", is(book.getPagesNumber())))
                .andExpect(jsonPath("$.rating", is(book.getRating())));


        verify(bookService, times(1)).getBook(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    void getBook_noIdMatch_returnNotFound() throws Exception {
        when(bookService.getBook(any())).thenReturn(Optional.empty());

        mockMvc.perform(get("/books/{id}", UUID.randomUUID().toString()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(bookService, times(1)).getBook(any());
        verifyNoMoreInteractions(bookService);
    }
}
