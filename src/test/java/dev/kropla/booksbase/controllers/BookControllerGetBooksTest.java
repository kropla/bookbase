package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.bootstrap.BootstrapBooksTestData;
import dev.kropla.booksbase.services.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static dev.kropla.booksbase.bootstrap.BootstrapBooksTestData.generateSingleBookDtoWithId;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BookController.class)
class BookControllerGetBooksTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    BookService bookService;

    @Test
    void getBooks_noRecords_returnsEmptyListWithOkStatus() throws Exception {
        when(bookService.getBooks(any())).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/books").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isEmpty());

        verify(bookService, times(1)).getBooks(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    void getBooks_recordsReturnedByService_returnsBookListWithOkStatus() throws Exception {
        var limit = 5;
        when(bookService.getBooks(any())).thenReturn(BootstrapBooksTestData.getBooksDto().stream().limit(limit).toList());

        mockMvc.perform(get("/books").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(limit)));

        verify(bookService, times(1)).getBooks(any());
        verifyNoMoreInteractions(bookService);
    }

    @Test
    void getBooks_recordReturnedByService_returnsBookWithCorrectStructure() throws Exception {
        var book = generateSingleBookDtoWithId();
        when(bookService.getBooks(any())).thenReturn(List.of(book));

        mockMvc.perform(get("/books").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(book.getId().toString())))
                .andExpect(jsonPath("$[0].title", is(book.getTitle())))
                .andExpect(jsonPath("$[0].author", is(book.getAuthor())))
                .andExpect(jsonPath("$[0].isbn", is(book.getIsbn())))
                .andExpect(jsonPath("$[0].pagesNumber", is(book.getPagesNumber())))
                .andExpect(jsonPath("$[0].rating", is(book.getRating())));


        verify(bookService, times(1)).getBooks(any());
        verifyNoMoreInteractions(bookService);
    }
}
