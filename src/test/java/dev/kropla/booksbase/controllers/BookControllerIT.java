package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.model.entities.Book;
import dev.kropla.booksbase.model.entities.Comment;
import dev.kropla.booksbase.repositories.BookRepository;
import dev.kropla.booksbase.repositories.CommentRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;

@ActiveProfiles({"it"})
@SpringBootTest
class BookControllerIT {

    @Autowired
    BookController bookController;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    CommentRepository commentRepository;

    @Test
    void getBooks_requestedSixElements_statusCodeIsOkWithSixBooksInResponse() {
        // Given
        int elementsSize = 6;
        Pageable page = PageRequest.of(0, elementsSize);

        // When
        ResponseEntity<List<BookDto>> response = bookController.getBooks(page);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotEmpty().hasSize(elementsSize);
    }

    @Rollback
    @Transactional
    @Test
    void getBooks_noDataOnDB_statusCodeIsOkWithEmptyListResponse() {
        // Given
        bookRepository.deleteAll();
        int elementsSize = 6;
        Pageable page = PageRequest.of(0, elementsSize);

        // When
        ResponseEntity<List<BookDto>> response = bookController.getBooks(page);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isEmpty();
    }

    @Test
    void getBook_requestedExistingBook_statusCodeIsOkWithBookInResponse() {
        // Given
        var bookFromDb = bookRepository.findAll().getFirst();

        // When
        ResponseEntity<BookDto> response = bookController.getBook(bookFromDb.getId());


        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
                .extracting("id", "title", "author", "pagesNumber", "rating", "isbn")
                .contains(bookFromDb.getId(), bookFromDb.getTitle(), bookFromDb.getAuthor(), bookFromDb.getPagesNumber(), bookFromDb.getRating(), bookFromDb.getIsbn());
    }

    @Rollback
    @Transactional
    @Test
    void getBook_requestedExistingBookWithComments_statusCodeIsOkWithBookWithLastFiveComments() {
        // Given
        var bookFromDb = bookRepository.findAll().getFirst();
        generateAndSaveCommentsForBook(bookFromDb);

        // When
        ResponseEntity<BookDto> response = bookController.getBook(bookFromDb.getId());

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).hasFieldOrProperty("lastComments");
        assertThat(Objects.requireNonNull(response.getBody()).getLastComments()).hasSize(5);
    }

    @Rollback
    @Transactional
    @Test
    void removeBook_removeExistingBook_statusCodeIsNoContentEntityRemoved() {
        // Given
        var bookFromDb = bookRepository.findAll().getFirst();
        generateAndSaveCommentsForBook(bookFromDb);
        assertThat(commentRepository.findFirst5ByBookIdOrderByCreatedAtDesc(bookFromDb.getId())).hasSize(5);

        // When
        ResponseEntity<Void> response = bookController.removeBook(bookFromDb.getId());

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(bookRepository.findById(bookFromDb.getId())).isEmpty();
        assertThat(commentRepository.findFirst5ByBookIdOrderByCreatedAtDesc(bookFromDb.getId())).isEmpty();
    }

    @Rollback
    @Transactional
    @Test
    void removeBook_removeExistingBookWithComments_statusCodeIsNoContentEntityRemovedWithComments() {
        // Given
        var bookFromDb = bookRepository.findAll().getFirst();

        // When
        ResponseEntity<Void> response = bookController.removeBook(bookFromDb.getId());


        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(bookRepository.findById(bookFromDb.getId())).isEmpty();
    }

    @Rollback
    @Transactional
    @Test
    void removeBook_removeNotExistingBook_notFoundExceptionIsThrown() {
        // Given
        var booksInDb = bookRepository.findAll();
        var newUUID = findNotExistingInDbId(booksInDb);

        assertThat(newUUID).isPresent();

        // When + Then
        assertThatExceptionOfType(NotFoundException.class)
                .isThrownBy(() -> bookController.removeBook(newUUID.get()));

        assertThat(booksInDb).hasSize((int) bookRepository.count());

    }

    @Rollback
    @Transactional
    @Test
    void createBook_correctEntity_newBookStoredInDb() {
        // Given
        var entitiesInDbBeforeCounter = bookRepository.count();

        var book = BookDto.builder().title("Test title").author("Test author").pagesNumber(10).rating(1).isbn("9786763598855").build();

        // When
        ResponseEntity<BookDto> response = bookController.createBook(book);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody()).isNotNull();
        assertThat(response.getBody().getId()).isNotNull();
        assertThat(bookRepository.count()).isEqualTo(entitiesInDbBeforeCounter + 1);
    }

    @Rollback
    @Transactional
    @Test
    void updateBook_correctEntity_updatedBookStoredInDb() {
        // Given
        var bookFromDb = bookRepository.findAll().getFirst();
        var oldTitle = bookFromDb.getTitle();
        var newTitle = oldTitle + "UPDATED";
        var newBook = BookDto.builder()
                .id(bookFromDb.getId())
                .title(newTitle)
                .author(bookFromDb.getAuthor())
                .rating(bookFromDb.getRating())
                .isbn(bookFromDb.getIsbn())
                .build();

        // When
        ResponseEntity<Void> response = bookController.updateBook(bookFromDb.getId(), newBook);

        // Then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isNull();

        // check if in DB changes are stored
        var updatedBook = bookRepository.findById(bookFromDb.getId());
        assertThat(updatedBook).isPresent();
        assertThat(updatedBook.get().getTitle()).isEqualTo(newTitle);
    }


    private void generateAndSaveCommentsForBook(Book bookFromDb) {
        var comments = IntStream.range(1, 10).mapToObj(i -> Comment.builder().text("test" + i).bookId(bookFromDb.getId()).build()).toList();

        commentRepository.saveAllAndFlush(comments);
    }

    /**
     * Find first random UUID that not exists in db entities.
     * If for some reason it won't be found in 10 times probe method will return Optional empty
     *
     * @return Optional with first UUID not in DB or empty when UUID can't be found
     */
    private Optional<UUID> findNotExistingInDbId(List<Book> booksInDb) {
        var booksIds = booksInDb.stream().map(Book::getId).collect(Collectors.toSet());
        var uuid = UUID.randomUUID();
        int i = 0;
        while (booksIds.contains(uuid) && i <= 10) {
            uuid = UUID.randomUUID();
            i++;
        }
        return i > 10 ? Optional.empty() : Optional.of(uuid);
    }

}