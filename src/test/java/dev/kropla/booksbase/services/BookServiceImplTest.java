package dev.kropla.booksbase.services;

import dev.kropla.booksbase.bootstrap.BootstrapBooksTestData;
import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.mappers.BookMapper;
import dev.kropla.booksbase.mappers.CommentMapper;
import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.model.entities.Book;
import dev.kropla.booksbase.model.entities.Comment;
import dev.kropla.booksbase.repositories.BookRepository;
import dev.kropla.booksbase.repositories.CommentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private BookMapper bookMapper;

    @Mock
    private CommentMapper commentMapper;

    @InjectMocks
    private BookServiceImpl bookService;


    @Test
    void getBooks_returnsBooksList() {
        var books = BootstrapBooksTestData.getBooks().stream().limit(5).toList();
        Pageable page = PageRequest.of(0, 5);
        Page<Book> pageWithBooks = new PageImpl<>(books);

        when(bookRepository.findAll(any(Pageable.class))).thenReturn(pageWithBooks);
        when(commentRepository.findFirst5ByBookIdOrderByCreatedAtDesc(any())).thenReturn(List.of(Comment.builder().text("asdf").build()));
        when(bookMapper.toDto(any())).thenReturn(BookDto.builder().build());
        when(commentMapper.toDto(any())).thenReturn(CommentDto.builder().build());

        var result = bookService.getBooks(page);

        assertThat(result).isNotEmpty().hasSize(5);
        verify(bookRepository, times(1)).findAll(any(Pageable.class));
        verify(commentRepository, times(5)).findFirst5ByBookIdOrderByCreatedAtDesc(any());
        verify(bookMapper, times(5)).toDto(any());
        verify(commentMapper, times(5)).toDto(any());

        verifyNoMoreInteractions(bookRepository);
        verifyNoMoreInteractions(commentRepository);
        verifyNoMoreInteractions(bookMapper);
        verifyNoMoreInteractions(commentMapper);
    }

    @Test
    void getBook_returnBook() {
        when(bookRepository.findById(any(UUID.class))).thenReturn(Optional.of(Book.builder().build()));
        when(commentRepository.findFirst5ByBookIdOrderByCreatedAtDesc(any())).thenReturn(List.of(Comment.builder().text("asdf").build()));
        when(bookMapper.toDto(any())).thenReturn(BookDto.builder().build());
        when(commentMapper.toDto(any())).thenReturn(CommentDto.builder().build());
        var expectedBookDto = BookDto.builder().lastComments(List.of(CommentDto.builder().build())).build();


        var result = bookService.getBook(UUID.randomUUID());

        assertThat(result).isPresent().contains(expectedBookDto);
        verify(bookRepository, times(1)).findById(any(UUID.class));
        verify(commentRepository, times(1)).findFirst5ByBookIdOrderByCreatedAtDesc(any());
        verify(bookMapper, times(1)).toDto(any());
        verify(commentMapper, times(1)).toDto(any());

        verifyNoMoreInteractions(bookRepository);
        verifyNoMoreInteractions(commentRepository);
        verifyNoMoreInteractions(bookMapper);
        verifyNoMoreInteractions(commentMapper);
    }

    @Test
    void getBook_returnEmpty() {
        when(bookRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        var result = bookService.getBook(UUID.randomUUID());

        assertThat(result).isEmpty();
        verify(bookRepository, times(1)).findById(any(UUID.class));

        verifyNoMoreInteractions(bookRepository);
        verifyNoInteractions(commentRepository);
        verifyNoInteractions(bookMapper);
        verifyNoInteractions(commentMapper);
    }

    @Test
    void removeBook_foundBookToRemove_returnTrue() {
        when(bookRepository.existsById(any(UUID.class))).thenReturn(true);

        var result = bookService.removeBook(UUID.randomUUID());

        assertThat(result).isTrue();
        verify(bookRepository, times(1)).existsById(any(UUID.class));
        verify(bookRepository, times(1)).deleteById(any(UUID.class));
        verify(commentRepository, times(1)).deleteAllByBookId(any(UUID.class));
        verifyNoMoreInteractions(bookRepository);
        verifyNoMoreInteractions(commentRepository);
        verifyNoInteractions(bookMapper);
        verifyNoInteractions(commentMapper);
    }

    @Test
    void removeBook_noBookInRepository_returnFalse() {
        when(bookRepository.existsById(any(UUID.class))).thenReturn(false);

        var result = bookService.removeBook(UUID.randomUUID());

        assertThat(result).isFalse();
        verify(bookRepository, times(1)).existsById(any(UUID.class));

        verifyNoMoreInteractions(bookRepository);
        verifyNoInteractions(commentRepository);
        verifyNoInteractions(bookMapper);
        verifyNoInteractions(commentMapper);
    }

    @Test
    void createBook_returnCreatedDto() {
        var bookDto = BookDto.builder().build();
        when(bookRepository.save(any())).thenReturn(Book.builder().build());
        when(bookMapper.toDto(any())).thenReturn(bookDto);
        when(bookMapper.toEntity(any())).thenReturn(Book.builder().build());

        var result = bookService.create(bookDto);

        assertThat(result).isNotNull().isEqualTo(bookDto);
        verify(bookRepository, times(1)).save(any());
        verify(bookMapper, times(1)).toDto(any());
        verify(bookMapper, times(1)).toEntity(any());
        verifyNoMoreInteractions(bookRepository);
        verifyNoMoreInteractions(bookMapper);
        verifyNoInteractions(commentMapper);
        verifyNoInteractions(commentRepository);
    }

    @Test
    void updateBook_returnChangedDto() {
        var bookDto = BookDto.builder().build();
        when(bookRepository.findById(any(UUID.class))).thenReturn(Optional.of(Book.builder().build()));
        when(bookRepository.save(any())).thenReturn(Book.builder().build());
        when(bookMapper.toDto(any())).thenReturn(bookDto);

        var result = bookService.update(UUID.randomUUID(), bookDto);

        assertThat(result).isNotNull().isPresent().contains(bookDto);
        verify(bookRepository, times(1)).findById(any());
        verify(bookRepository, times(1)).save(any());
        verify(bookMapper, times(1)).toDto(any());
        verifyNoMoreInteractions(bookRepository);
        verifyNoMoreInteractions(bookMapper);
        verifyNoInteractions(commentMapper);
        verifyNoInteractions(commentRepository);
    }

    @Test
    void updateBook_bookNotFoundInDb_returnsEmpty() {
        var bookDto = BookDto.builder().build();
        var uuid = UUID.randomUUID();
        when(bookRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        var result = bookService.update(uuid, bookDto);

        assertThat(result).isEmpty();
        verify(bookRepository, times(1)).findById(any());
        verifyNoMoreInteractions(bookRepository);
        verifyNoInteractions(bookMapper);
        verifyNoInteractions(commentMapper);
        verifyNoInteractions(commentRepository);
    }

}