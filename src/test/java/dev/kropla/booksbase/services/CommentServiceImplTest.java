package dev.kropla.booksbase.services;

import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.mappers.CommentMapper;
import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.model.entities.Book;
import dev.kropla.booksbase.model.entities.Comment;
import dev.kropla.booksbase.repositories.BookRepository;
import dev.kropla.booksbase.repositories.CommentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CommentServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private CommentMapper commentMapper;

    @InjectMocks
    private CommentServiceImpl commentService;


    @Test
    void addComment_bookExists_commentSaved() {
        var commentDto = CommentDto.builder().build();
        var commentEntity = Comment.builder().build();
        when(bookRepository.findById(any(UUID.class))).thenReturn(Optional.of(Book.builder().build()));
        when(commentRepository.save(any())).thenReturn(commentEntity);
        when(commentMapper.toDto(any())).thenReturn(commentDto);
        when(commentMapper.toEntity(any())).thenReturn(commentEntity);

        var result = commentService.addComment(UUID.randomUUID(), commentDto);

        assertThat(result).isNotNull().isEqualTo(commentDto);
        verify(bookRepository, times(1)).findById(any());
        verify(commentRepository, times(1)).save(any());
        verify(commentMapper, times(1)).toDto(any());
        verify(commentMapper, times(1)).toEntity(any());

        verifyNoMoreInteractions(bookRepository);
        verifyNoMoreInteractions(commentRepository);
        verifyNoMoreInteractions(commentMapper);
    }

    @Test
    void addComment_bookNotExists_throwNotFoundException() {
        var commentDto = CommentDto.builder().build();
        var uuid = UUID.randomUUID();
        when(bookRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> {
            commentService.addComment(uuid, commentDto);
        });

        verify(bookRepository, times(1)).findById(any());

        verifyNoMoreInteractions(bookRepository);
        verifyNoInteractions(commentRepository);
        verifyNoInteractions(commentMapper);
    }

}