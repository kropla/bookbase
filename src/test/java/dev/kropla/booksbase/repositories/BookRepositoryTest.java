package dev.kropla.booksbase.repositories;

import dev.kropla.booksbase.model.entities.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class BookRepositoryTest {

    @Autowired
    BookRepository bookRepository;

    @Test
    void testSaveBook() {
        String bookTitle = "Test book title";

        Book savedBook = bookRepository.save(Book.builder().title(bookTitle).build());

        assertThat(savedBook).isNotNull();
        assertThat(savedBook.getId()).isNotNull();
        assertThat(savedBook.getTitle()).isEqualTo(bookTitle);

    }

}