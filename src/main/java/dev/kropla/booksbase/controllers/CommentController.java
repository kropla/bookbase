package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.services.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Tag(name = "Book Comments API")
@RestController
@RequestMapping("/books/{bookId}/comments")
@RequiredArgsConstructor
public class CommentController {

    private final CommentService commentService;

    @Operation(summary = "Create new comment", description = "Create new comment to specific book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "New comment created"),
    })
    @PostMapping
    public ResponseEntity<CommentDto> addCommentToBook(@NotNull @PathVariable UUID bookId, @Validated @RequestBody CommentDto newComment) {

        CommentDto addedComment = commentService.addComment(bookId, newComment);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(addedComment.getBookId(), addedComment.getId())
                .toUri();

        return ResponseEntity.created(location).body(addedComment);
    }

}
