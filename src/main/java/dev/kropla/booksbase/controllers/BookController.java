package dev.kropla.booksbase.controllers;

import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.services.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Tag(name = "Book Inventory API")
@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @Operation(summary = "Get all books", description = "Returns a books information from inventory with comments")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned list of books"),
    })
    @GetMapping
    @PageableAsQueryParam
    public ResponseEntity<List<BookDto>> getBooks(@Parameter(hidden = true) Pageable pageable) {
        return ResponseEntity.ok(bookService.getBooks(pageable));
    }

    @Operation(summary = "Get a book", description = "Returns a book details by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved book"),
            @ApiResponse(responseCode = "404", description = "Not found - The book was not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<BookDto> getBook(@NotNull @PathVariable("id") UUID id) {

        return bookService.getBook(id).map(ResponseEntity::ok).orElseThrow(NotFoundException::new);
    }

    @Operation(summary = "Delete a book", description = "Delete a book with a given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Removed book by given id"),
            @ApiResponse(responseCode = "404", description = "Book to delete not found"),
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> removeBook(@PathVariable("id") UUID id) {

        if (!bookService.removeBook(id)) {
            throw new NotFoundException();
        }

        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Create a book", description = "Create new book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Create new book"),
    })
    @PostMapping
    public ResponseEntity<BookDto> createBook(@Validated @RequestBody BookDto bookDto) {
        BookDto createdBookDto = bookService.create(bookDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(createdBookDto.getId()).toUri();

        return ResponseEntity.created(location).body(createdBookDto);
    }

    @Operation(summary = "Update book", description = "Update existing book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Update process finished and book was updated"),
            @ApiResponse(responseCode = "404", description = "Book to update was not found"),
    })
    @PutMapping("/{id}")
    public ResponseEntity<Void> updateBook(@NotNull @PathVariable UUID id, @Validated @RequestBody BookDto bookDto) {

        if (bookService.update(id, bookDto).isEmpty()) {
            throw new NotFoundException();
        }

        return ResponseEntity.noContent().build();
    }
}

