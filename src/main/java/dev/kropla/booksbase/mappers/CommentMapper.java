package dev.kropla.booksbase.mappers;

import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.model.entities.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface CommentMapper {

    Comment toEntity(CommentDto dto);

    @Mapping(target = "bookId", ignore = true)
    CommentDto toDto(Comment comment);

}
