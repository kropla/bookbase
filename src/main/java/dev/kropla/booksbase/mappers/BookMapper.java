package dev.kropla.booksbase.mappers;

import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.model.entities.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface BookMapper {

    Book toEntity(BookDto dto);

    @Mapping(target = "lastComments", ignore = true)
    BookDto toDto(Book book);

}
