package dev.kropla.booksbase.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private UUID id;

    @NotBlank(message = "Text message can't be blank")
    @Size(min = 2, message = "Text message too short (min 2 characters)")
    @Size(max = 255, message = "Comment text too long (max 255 characters)")
    private String text;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    @PastOrPresent(message = "Comment can't be created with future date")
    private Instant createdAt;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private UUID bookId;
}
