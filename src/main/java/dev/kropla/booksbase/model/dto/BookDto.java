package dev.kropla.booksbase.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import dev.kropla.booksbase.validators.IsbnValidation;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookDto {

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private UUID id;

    @NotBlank(message = "Book title can't be blank")
    private String title;

    @NotBlank(message = "Author name can't be blank")
    private String author;

    @Positive(message = "Number of pages needs to be positive number")
    private int pagesNumber;

    @NotNull(message = "Book rating can't be null")
    @Min(value = 1, message = "Book rating number to low")
    @Max(value = 5, message = "Book rating number to high")
    private int rating;

    @NotBlank(message = "Book ISBN can't be blank")
    @IsbnValidation(message = "Book ISBN wrong format")
    private String isbn;

    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    List<CommentDto> lastComments;

}
