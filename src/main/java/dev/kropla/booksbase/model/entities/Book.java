package dev.kropla.booksbase.model.entities;

import dev.kropla.booksbase.validators.IsbnValidation;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;

import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(length = 36, updatable = false, nullable = false)
    private UUID id;

    @NotBlank
    private String title;

    @NotBlank
    private String author;

    @Positive
    private int pagesNumber;

    @NotNull
    @Min(1)
    @Max(5)
    private int rating;

    @IsbnValidation
    private String isbn;

    @Version
    private Integer version;
}
