package dev.kropla.booksbase.services;

import dev.kropla.booksbase.model.dto.BookDto;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


public interface BookService {

    List<BookDto> getBooks(Pageable pageable);

    Optional<BookDto> getBook(UUID id);

    boolean removeBook(UUID id);

    BookDto create(BookDto bookDto);

    Optional<BookDto> update(UUID id, BookDto bookDto);
}
