package dev.kropla.booksbase.services;

import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.mappers.BookMapper;
import dev.kropla.booksbase.mappers.CommentMapper;
import dev.kropla.booksbase.model.dto.BookDto;
import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.repositories.BookRepository;
import dev.kropla.booksbase.repositories.CommentRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@RequiredArgsConstructor
@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final CommentRepository commentRepository;
    private final BookMapper bookMapper;
    private final CommentMapper commentMapper;

    @Override
    public List<BookDto> getBooks(Pageable pageable) {
        var books = bookRepository.findAll(pageable)
                .stream()
                .map(bookMapper::toDto)
                .toList();

        books.forEach(b -> b.setLastComments(
                getLastComments(b)
        ));

        return books;
    }

    @Override
    public Optional<BookDto> getBook(UUID id) {
        var optionalBook = bookRepository.findById(id);

        if (optionalBook.isEmpty()) {
            return Optional.empty();
        }

        var bookDto = bookMapper.toDto(optionalBook.get());
        bookDto.setLastComments(
                getLastComments(bookDto));

        return Optional.of(bookDto);
    }

    @Transactional
    @Override
    public boolean removeBook(UUID id) {
        if (bookRepository.existsById(id)) {
            bookRepository.deleteById(id);
            commentRepository.deleteAllByBookId(id);
            return true;
        }
        return false;
    }

    @Override
    public BookDto create(BookDto bookDto) {
        return saveDto(bookDto);
    }

    @Override
    public Optional<BookDto> update(@NotNull UUID id, BookDto bookDto) {
        AtomicReference<Optional<BookDto>> atomicReference = new AtomicReference<>();

        bookRepository.findById(id).ifPresentOrElse(foundBook -> {
            foundBook.setTitle(bookDto.getTitle());
            foundBook.setAuthor(bookDto.getAuthor());
            foundBook.setPagesNumber(bookDto.getPagesNumber());
            foundBook.setRating(bookDto.getRating());
            foundBook.setIsbn(bookDto.getIsbn());
            atomicReference.set(Optional.of(bookMapper
                    .toDto(bookRepository.save(foundBook))));
        }, () -> atomicReference.set(Optional.empty()));

        return atomicReference.get();
    }

    private List<CommentDto> getLastComments(BookDto b) {
        return commentRepository.findFirst5ByBookIdOrderByCreatedAtDesc(b.getId())
                .stream()
                .map(commentMapper::toDto)
                .toList();
    }

    private BookDto saveDto(BookDto bookDto) {
        return bookMapper.toDto(
                bookRepository.save(bookMapper.toEntity(bookDto))
        );
    }
}
