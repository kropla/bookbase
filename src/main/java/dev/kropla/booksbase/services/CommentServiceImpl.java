package dev.kropla.booksbase.services;

import dev.kropla.booksbase.exceptions.NotFoundException;
import dev.kropla.booksbase.mappers.CommentMapper;
import dev.kropla.booksbase.model.dto.CommentDto;
import dev.kropla.booksbase.model.entities.Book;
import dev.kropla.booksbase.repositories.BookRepository;
import dev.kropla.booksbase.repositories.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class CommentServiceImpl implements CommentService {

    private final BookRepository bookRepository;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    @Override
    public CommentDto addComment(UUID bookId, CommentDto comment) {
        Optional<Book> optionalBook = bookRepository.findById(bookId);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            comment.setBookId(book.getId());
            var savedEntity = commentRepository.save(commentMapper.toEntity(comment));
            return commentMapper.toDto(savedEntity);
        } else {
            throw new NotFoundException("Book not found with id: " + bookId);
        }
    }
}
