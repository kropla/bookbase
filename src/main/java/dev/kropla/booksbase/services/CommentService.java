package dev.kropla.booksbase.services;

import dev.kropla.booksbase.model.dto.CommentDto;

import java.util.UUID;

public interface CommentService {
    CommentDto addComment(UUID bookId, CommentDto comment);
}
