package dev.kropla.booksbase.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = IsbnValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsbnValidation {
    String message() default "Invalid ISBN number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
