package dev.kropla.booksbase.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.apache.commons.validator.routines.ISBNValidator;

public class IsbnValidator implements ConstraintValidator<IsbnValidation, String> {

    @Override
    public boolean isValid(String isbnValue, ConstraintValidatorContext context) {
        var validator = new ISBNValidator();
        return isbnValue != null && validator.isValid(isbnValue);
    }
}
