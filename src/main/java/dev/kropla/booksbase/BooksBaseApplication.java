package dev.kropla.booksbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BooksBaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BooksBaseApplication.class, args);
	}

}
