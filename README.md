# Building application

```shell
  mvn clean package
```

# Running integration tests
- For running integration tests `it` spring profile is used. It uses h2 db, inserts test data and performs tests
```shell
  mvn clean verify
```

# Running application

Application uses database connection. There is defined connection to postgres DB. 
You can run application with local running instance of postgresDB (standalone), the one created with docker or just use 
specific spring docker compose support.
- Connection credentials can be found in the `application.properties` file.
- For production version those credentials should be removed from properties file - this is just small POC.


## Three ways of running application based on DB installation

### Choose one of the following: 

### 1. Running with standalone DB
- Make sure that your DB is available under `localhost` on port `5432`. This is default postgres port. 
- If your DB runs on different port or host make changes in `application.properties`
- Run application with command (the same as with standalone DB):
```shell
    mvn spring-boot:run
```


### 2. Running with DB in docker (docker installation needed)
- You need to have docker install on your machine
- To run DB in docker you need to pull postgres image and run it with terminal commands:
```shell
    docker pull postgres:16
    docker run --name postgresdb -e POSTGRES_PASSWORD=1234 -d postgres
```
- Alternatively you can run services that are define in `compose.yaml` file on root directory
- Run application with command (the same as with standalone DB):
```shell
    mvn spring-boot:run
```

### 3. Running application with use of spring docker compose support (docker installation needed)
- You need to have docker install on your machine
- You can run application without checking any DB connectivity or pulling DB manually - just use docker compose support

```shell
  mvn spring-boot:run -Dspring-boot.run.profiles=dockerdb
```


# Testing application
When application is up and running you can test it using `newman` (CLI version of the `postman`) or just `postman`.

### Testing using newman
- You need to have `newman` installed
```shell
    npm install -g newman
```
OR for macOS
```shell
  brew install newman
```

- Running tests:
```shell
  cd postman
  newman run books.postman_collection.json -e LOCAL.postman_environment.json
```
#### Result of the newman collection run should look similar like that:
![alt newman](./doc/newman_summary.png)

### Testing using postman
- test collection can be found in `postman` directory under the project main directory
- import collection from file: `books.postman_collection.json`
- import environment settings: `LOCAL.postman_environment.json` or set environments locally: 
 
    `base_url` - used in all tests to point to host and port on which app is running (usually it is `localhost:8080`)
    `book_id` - used across the tests to set correct id for book entity used in tests

#### Postman test collection:
  ![alt postman collection](./doc/postman_collection_tests.png)

#### Postman example result:
  ![alt postman collection result](./doc/postman_collection_run_result.png)

### Each postman call has their own set of test cases:
  ![alt postman test js](./doc/example_postman_js_test.png)



# Swagger support
- after running application Swagger console is available under the URL: http://localhost:8080/swagger-ui/index.html
  
#### Swagger console:
  ![alt text](./doc/swagger_console.png)